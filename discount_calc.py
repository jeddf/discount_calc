"""Discount calculator, thinkful project."""
class DiscountCalc(object):

    def cartDisc(self, cartc, dcount, dtype):
        if dtype == 'percent':
            if dcount > 100:
                raise ValueError("Discounts over 100% not valid")
            return round((float(cartc) * (float(dcount)/100)), 2)
        if dtype == 'absolute':
            if dcount > cartc:
                raise ValueError("Discount greater than cart value, not valid.")
            else:
                return dcount
        else:
            raise ValueError("Invalid discount type.")
        
