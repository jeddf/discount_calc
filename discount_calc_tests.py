"""Tests for discount calculator"""
import unittest
from discount_calc import DiscountCalc

class discunittester(unittest.TestCase):
    def setUp(self):
        self.disc = DiscountCalc()
    def ten_percent_discount_test(self):
        result = self.disc.cartDisc(100,10,'percent')
        self.assertEqual(10, result)
    def fifteen_percent_discount_test(self):
        result = self.disc.cartDisc(100,15,'percent')
        self.assertEqual(15, result)
    def five_dollar_discount_test(self):
        result = self.disc.cartDisc(250,5,'absolute')
        self.assertEqual(5, result)
    def dicount_type_error_test(self):
        self.assertRaises(ValueError, self.disc.cartDisc, 250, 5, 'rondom')
    def float_percentage_discount_round_test(self):
        result = self.disc.cartDisc(45, 12.5, 'percent')
        self.assertEqual(5.63, result)
    def float_absolute_discount_test(self):
        result = self.disc.cartDisc(340, 50.51, 'absolute')
    def excess_percentage_discount_test(self):
        self.assertRaises(ValueError, self.disc.cartDisc, 100, 120, 'percent')
    def excess_dollar_discount_test(self):
        self.assertRaises(ValueError, self.disc.cartDisc, 40, 50, 'absolute')
